### (Note Re: Foundry v12)
The Dark Matter mods I've done have been just for me. The only reason I'm here is because someone on Reddit asked if I could get the modules to load in v12. Apparently the work I've done has been a time saver, and that one person was enough to bring me here to try and at least get stuff loading again.

I was able to poke RabidOwlbear, and he went as far as to fix the [Dark Matter Extension](https://gitlab.com/dark-matter1/dme) script that generates random planets, and is now trying to fix the stylesheets which is pretty kind of him because the dnd5e system has changed so much in a few years.

I'm going to get [Grax's](https://gitlab.com/dark-matter1/dm-grax) to load, and maybe [Conspiracy](https://gitlab.com/dark-matter1/dm-cits), even though the later is in the former, but at least they should give people a starting point for more easily running these cool adventures.

Hope someone gets to use this and share it with some friends, and it doesn't explode.
-A.

![Supported](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat-square%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fdark-matter1%2Fdm-grax%2F-%2Fraw%2Fmain%2Fmodule.json) ![Game System](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fsystem%3FnameType%3Dfull%26showVersion%3D1%26style%3Dflat-square%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fdark-matter1%2Fdm-grax%2F-%2Fraw%2Fmain%2Fmodule.json)

# Dark Matter: Grax's Club

![](images/graxs-club.jpg)

### 6, THRILLING Adventures for 5e Characters, set in the Dark Matter 'Verse!:

1. Bipinbop’s Factory of Fun - AN ADVENTURE INTO A FANTASTICAL FACTORY, WRITTEN BY 
   [MIKE HOLIK](mailto:mike@magehandpress.com), [MAGE HAND PRESS](https://store.magehandpress.com/)
2. Conspiracy in the Stars! - AN NOIR THRILLER ADVENTURE, WRITTEN BY 
   [MIKE HOLIK](mailto:mike@magehandpress.com), [DONATHIN FRYE](https://donathinfrye.com/), CHARLIE SMITH,
   SIMON GOUDREAULT, and [MAKENZIE DE ARMAS](https://www.makenzielaneda.com/).
3. The Even Most Dangerous-er Game - AN ADVENTURE THROUGH THE DEEP JUNGLE FOR 5TH-LEVEL CHARACTERS, 
   WRITTEN BY [MIKE HOLIK](mailto:mike@magehandpress.com), [MAGE HAND PRESS](https://store.magehandpress.com/)
4. Curse of the Astromo - A DEEP SPACE SURVIVAL HORROR ADVENTURE FOR 6TH-LEVEL CHARACTERS, WRITTEN BY 
   [DONATHIN FRYE](https://donathinfrye.com/).
5. The Pink Star Scandal - AN INVESTIGATIVE COURTOOM ADVENTURE FOR 7TH-LEVEL CHARACTERS, WRITTEN BY 
   [LILI SPARX](https://magpiegames.com/pages/lili-sparx).
6. ‘dex Education - A FIELD TRIP ADVENTURE, WRITTEN BY [MAKENZIE DE ARMAS](https://www.makenzielaneda.com/).

## Installation
1. From within Foundry VTT Configuration and Setup Screen, select the Add-on Modules option.
2. Copy the link for the [Manifest URL](https://gitlab.com/dark-matter1/dm-grax/-/raw/main/module.json) into the 
   same field at the bottom of the window.
3. Create a game world using the [Foundry VTT dnd5e Game System](https://gitlab.com/foundrynet/dnd5e), or open 
   an existing world.
4. Enable the module and required dependancies under Game Settings > Manage Modules.

Data should be copied from compendiums into the game world via Scene Packer. 
If not, you can trigger Scene Packer by finding Grax's Club in the module settings Foundry configuration menu.

## Dependencies
This module _requires_ some other extensions to run properly.

* [Dark Matter Extensions (dme)](https://gitlab.com/dark-matter1/dme) adds Dark Matter specific skills, weapon 
  properties and adds some Dark Matter touches to the Foundry UI.
* [Dark Matter Compendium](https://gitlab.com/dark-matter1/dark-matter-compendium) adds data from the core rulebook.
* [Library: Scene Packer](https://foundryvtt.com/packages/scene-packer) A library to help content creators package 
  up Scenes and Adventures to solve several frustrations when importing Scenes from a module compendium.
* [Journal Anchor Links](https://foundryvtt.com/packages/jal) lets you link to a header (h1-h6) in a journal page. 
  Makes it easy to have scene notes that open to a place in a journal entry.
* [Compendium Folders](https://github.com/earlSt1/vtt-compendium-folders) adds subfolders to the 
    compendiums menu.
  * _(Optional)_ [Forien's Quest Log](https://foundryvtt.com/packages/forien-quest-log/) adds a quest log for 
    players/GM. This add-on is currently not updated for Foundry v8/9, but it works. Quests are contained in the
    _fql_quests folder in Grax's Journals Compendium.

### Notice: This is a ***fan made*** add-on.
* It is **not** for sale/resale, **not** official, and should *not replace the official MHP module*.
  * [Support the artists](https://store.magehandpress.com/), and [buy the books](https://dark-matter.backerkit.com/hosted_preorders)!
  * Feel free to send Ko-fi for my pending legal troubles.
  * [![](images/kofi_button_blue-sm.png)](https://ko-fi.com/android8675)

If you want to contribute, feel free to reach out out to one of the people mentioned here,
 in Discord at the [Mage Hand Press Server](https://discord.gg/pJEWa6b).

