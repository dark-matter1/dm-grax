# 0.6.0
* I've stalled working on this module. I'd love to get some submissions to add to it. Maps, tokens, etc.
* Hey, if you open an issue at [the issue page](https://gitlab.com/dark-matter1/dm-grax/-/issues), I'll take a crack at it.

## 0.5.1
* Could always use help from anyone who's good at cutting and pasting.
* Found some content parsers that let you cut and paste from PDFs and will generate items/actors. Hoping it'll make things run a little smoother.

# 0.5.0
* _2022/10/03_ - v10 From this point forward.
* Look, if you have NOT started a campaign yet, you should get on Foundry v10. There may be SOME issues with v10, but they are being cleaned up QUICKLY.

## 0.4.7 - Last build v9.280 referenced.
* Going forward with v0.5.x it'll be v10 centric. So don't go past this point unless you MEAN it.

## 0.4.6
* Some minor tweaks for Foundry V9.
* Now that v10 is out and has all sorts of issues, any version up to 0.4.9 will be V9, and v10 from 0.5.0 forward.
* If in doubt, ask me and I'll point you to the last v9 build, but that shouldn't be many of you.

## 0.4.4
* _2022/01/17_ - We're public. Lord help us.
* This module is a work in progress. The most obivous thing missing is Scene's.
  * Feel free to help in that regard, **hint hint**!
* Stop by the [Issues Page](https://gitlab.com/dark-matter1/dm-grax/-/issues) if you have any bugs, suggestions or random thoughts!

### 0.4.3
* 2022/01/03 - Happy New Year!
* Added most of the NPCs
* Used placeholder tokens IF there is no official token from MHP.
* Some older entries use random art from the Internet.
* These should probably be removed for placeholders until offical art gets released.
* Next up is there's a ton of stuff missing from Dark-Matter-Compendiums (dmc).
* dmc is a mess. It needs cleaning. I wish I could get some help with that.

### 0.4.1
* 2021/12/21 - Started adding support for [Forien's Quest Log](https://github.com/League-of-Foundry-Developers/foundryvtt-forien-quest-log) add-on.

### 0.4
* 2021/11/30 - Started doing rolltables for the Broken Krash Machine.
* Several hours later, and I had to add magic items that were not entered into Dark Matter Compendium module.
* If you get a bunch of broken links in Appendix E, update Dark Matter Compendium.

### 0.3.1
* 2021/11/11 - Minor fixes

### 0.3
* 2021/11/10 + Got MOST, if not all of the PDF copied into Journal Entries.
  * Added some of the b/w maps into images/maps and linked them to journals where appropriate.
  * Next up is making NPC/PC/Monster character sheets and finding appropriate Tokens.
  * Stuff will be linked to journals, and anything referenced in the original dark-matter-compendium mod will be checked for accuracy.

### 0.2
* 2021/11/28 - Just found Grax's Club v1.0 which has all 5 adventures, will be adding them today, at least starting to copy in the Journal Data.

